EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 4450 5950 0    50   Input ~ 0
C0
Text GLabel 4450 6050 0    50   Input ~ 0
C1
Text GLabel 4450 6150 0    50   Input ~ 0
C2
Text GLabel 4450 6250 0    50   Input ~ 0
C3
Text GLabel 4450 6350 0    50   Input ~ 0
C4
Text GLabel 4450 6450 0    50   Input ~ 0
C5
Text GLabel 4450 6550 0    50   Input ~ 0
C6
Text GLabel 4450 6650 0    50   Input ~ 0
C7
Text GLabel 4450 6750 0    50   Input ~ 0
C8
Text GLabel 4450 6850 0    50   Input ~ 0
C9
$Comp
L Connector:Conn_01x04_Female J1
U 1 1 5FA9132B
P 3150 6400
F 0 "J1" H 3178 6376 50  0000 L CNN
F 1 "Conn_01x04_Female" H 3178 6285 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 3150 6400 50  0001 C CNN
F 3 "~" H 3150 6400 50  0001 C CNN
	1    3150 6400
	1    0    0    -1  
$EndComp
Text GLabel 2950 6300 0    50   Input ~ 0
R0
Text GLabel 2950 6400 0    50   Input ~ 0
R1
Text GLabel 2950 6500 0    50   Input ~ 0
R2
Text GLabel 2950 6600 0    50   Input ~ 0
R3
$Comp
L Switch:SW_Push SW3
U 1 1 5F82045A
P 3950 3350
F 0 "SW3" H 3950 3635 50  0000 C CNN
F 1 "SW_Push" H 3950 3544 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 3950 3550 50  0001 C CNN
F 3 "~" H 3950 3550 50  0001 C CNN
	1    3950 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D3
U 1 1 5F822077
P 3750 3450
F 0 "D3" V 3796 3380 50  0000 R CNN
F 1 "D" V 3705 3380 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 3750 3450 50  0001 C CNN
F 3 "~" V 3750 3450 50  0001 C CNN
	1    3750 3450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW4
U 1 1 5F840766
P 4550 3350
F 0 "SW4" H 4550 3635 50  0000 C CNN
F 1 "SW_Push" H 4550 3544 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 4550 3550 50  0001 C CNN
F 3 "~" H 4550 3550 50  0001 C CNN
	1    4550 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D4
U 1 1 5F84076C
P 4350 3450
F 0 "D4" V 4396 3380 50  0000 R CNN
F 1 "D" V 4305 3380 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 4350 3450 50  0001 C CNN
F 3 "~" V 4350 3450 50  0001 C CNN
	1    4350 3450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW15
U 1 1 5F84197C
P 3950 3850
F 0 "SW15" H 3950 4135 50  0000 C CNN
F 1 "SW_Push" H 3950 4044 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 3950 4050 50  0001 C CNN
F 3 "~" H 3950 4050 50  0001 C CNN
	1    3950 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D15
U 1 1 5F841982
P 3750 3950
F 0 "D15" V 3796 3880 50  0000 R CNN
F 1 "D" V 3705 3880 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 3750 3950 50  0001 C CNN
F 3 "~" V 3750 3950 50  0001 C CNN
	1    3750 3950
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW16
U 1 1 5F8423C6
P 4550 3850
F 0 "SW16" H 4550 4135 50  0000 C CNN
F 1 "SW_Push" H 4550 4044 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 4550 4050 50  0001 C CNN
F 3 "~" H 4550 4050 50  0001 C CNN
	1    4550 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D16
U 1 1 5F8423CC
P 4350 3950
F 0 "D16" V 4396 3880 50  0000 R CNN
F 1 "D" V 4305 3880 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 4350 3950 50  0001 C CNN
F 3 "~" V 4350 3950 50  0001 C CNN
	1    4350 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4150 3350 4150 3850
Wire Wire Line
	4750 3350 4750 3850
Wire Wire Line
	3750 3550 4350 3550
Wire Wire Line
	3750 4050 4350 4050
$Comp
L Switch:SW_Push SW27
U 1 1 5F85C2DA
P 3950 4350
F 0 "SW27" H 3950 4635 50  0000 C CNN
F 1 "SW_Push" H 3950 4544 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 3950 4550 50  0001 C CNN
F 3 "~" H 3950 4550 50  0001 C CNN
	1    3950 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D27
U 1 1 5F85C2E0
P 3750 4450
F 0 "D27" V 3796 4380 50  0000 R CNN
F 1 "D" V 3705 4380 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 3750 4450 50  0001 C CNN
F 3 "~" V 3750 4450 50  0001 C CNN
	1    3750 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW28
U 1 1 5F85C2E6
P 4550 4350
F 0 "SW28" H 4550 4635 50  0000 C CNN
F 1 "SW_Push" H 4550 4544 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 4550 4550 50  0001 C CNN
F 3 "~" H 4550 4550 50  0001 C CNN
	1    4550 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D28
U 1 1 5F85C2EC
P 4350 4450
F 0 "D28" V 4396 4380 50  0000 R CNN
F 1 "D" V 4305 4380 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 4350 4450 50  0001 C CNN
F 3 "~" V 4350 4450 50  0001 C CNN
	1    4350 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW39
U 1 1 5F85C2F2
P 3950 4850
F 0 "SW39" H 3950 5135 50  0000 C CNN
F 1 "SW_Push" H 3950 5044 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 3950 5050 50  0001 C CNN
F 3 "~" H 3950 5050 50  0001 C CNN
	1    3950 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D39
U 1 1 5F85C2F8
P 3750 4950
F 0 "D39" V 3796 4880 50  0000 R CNN
F 1 "D" V 3705 4880 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 3750 4950 50  0001 C CNN
F 3 "~" V 3750 4950 50  0001 C CNN
	1    3750 4950
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW40
U 1 1 5F85C2FE
P 4550 4850
F 0 "SW40" H 4550 5135 50  0000 C CNN
F 1 "SW_Push" H 4550 5044 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 4550 5050 50  0001 C CNN
F 3 "~" H 4550 5050 50  0001 C CNN
	1    4550 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D40
U 1 1 5F85C304
P 4350 4950
F 0 "D40" V 4396 4880 50  0000 R CNN
F 1 "D" V 4305 4880 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 4350 4950 50  0001 C CNN
F 3 "~" V 4350 4950 50  0001 C CNN
	1    4350 4950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4150 4350 4150 4850
Wire Wire Line
	4750 4350 4750 4850
Wire Wire Line
	3750 4550 4350 4550
Wire Wire Line
	3750 5050 4350 5050
Wire Wire Line
	4150 3850 4150 4350
Connection ~ 4150 3850
Connection ~ 4150 4350
Wire Wire Line
	4750 3850 4750 4350
Connection ~ 4750 3850
Connection ~ 4750 4350
$Comp
L Switch:SW_Push SW1
U 1 1 5F86BAD6
P 2750 3350
F 0 "SW1" H 2750 3635 50  0000 C CNN
F 1 "SW_Push" H 2750 3544 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 2750 3550 50  0001 C CNN
F 3 "~" H 2750 3550 50  0001 C CNN
	1    2750 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D1
U 1 1 5F86BAE0
P 2550 3450
F 0 "D1" V 2596 3380 50  0000 R CNN
F 1 "D" V 2505 3380 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 2550 3450 50  0001 C CNN
F 3 "~" V 2550 3450 50  0001 C CNN
	1    2550 3450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW2
U 1 1 5F86BAEA
P 3350 3350
F 0 "SW2" H 3350 3635 50  0000 C CNN
F 1 "SW_Push" H 3350 3544 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 3350 3550 50  0001 C CNN
F 3 "~" H 3350 3550 50  0001 C CNN
	1    3350 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D2
U 1 1 5F86BAF4
P 3150 3450
F 0 "D2" V 3196 3380 50  0000 R CNN
F 1 "D" V 3105 3380 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 3150 3450 50  0001 C CNN
F 3 "~" V 3150 3450 50  0001 C CNN
	1    3150 3450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW13
U 1 1 5F86BAFE
P 2750 3850
F 0 "SW13" H 2750 4135 50  0000 C CNN
F 1 "SW_Push" H 2750 4044 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 2750 4050 50  0001 C CNN
F 3 "~" H 2750 4050 50  0001 C CNN
	1    2750 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D13
U 1 1 5F86BB08
P 2550 3950
F 0 "D13" V 2596 3880 50  0000 R CNN
F 1 "D" V 2505 3880 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 2550 3950 50  0001 C CNN
F 3 "~" V 2550 3950 50  0001 C CNN
	1    2550 3950
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW14
U 1 1 5F86BB12
P 3350 3850
F 0 "SW14" H 3350 4135 50  0000 C CNN
F 1 "SW_Push" H 3350 4044 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 3350 4050 50  0001 C CNN
F 3 "~" H 3350 4050 50  0001 C CNN
	1    3350 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D14
U 1 1 5F86BB1C
P 3150 3950
F 0 "D14" V 3196 3880 50  0000 R CNN
F 1 "D" V 3105 3880 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 3150 3950 50  0001 C CNN
F 3 "~" V 3150 3950 50  0001 C CNN
	1    3150 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2950 3350 2950 3850
Wire Wire Line
	3550 3350 3550 3850
Wire Wire Line
	2550 3550 3150 3550
Wire Wire Line
	2550 4050 3150 4050
$Comp
L Switch:SW_Push SW25
U 1 1 5F86BB2A
P 2750 4350
F 0 "SW25" H 2750 4635 50  0000 C CNN
F 1 "SW_Push" H 2750 4544 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 2750 4550 50  0001 C CNN
F 3 "~" H 2750 4550 50  0001 C CNN
	1    2750 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D25
U 1 1 5F86BB34
P 2550 4450
F 0 "D25" V 2596 4380 50  0000 R CNN
F 1 "D" V 2505 4380 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 2550 4450 50  0001 C CNN
F 3 "~" V 2550 4450 50  0001 C CNN
	1    2550 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW26
U 1 1 5F86BB3E
P 3350 4350
F 0 "SW26" H 3350 4635 50  0000 C CNN
F 1 "SW_Push" H 3350 4544 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 3350 4550 50  0001 C CNN
F 3 "~" H 3350 4550 50  0001 C CNN
	1    3350 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D26
U 1 1 5F86BB48
P 3150 4450
F 0 "D26" V 3196 4380 50  0000 R CNN
F 1 "D" V 3105 4380 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 3150 4450 50  0001 C CNN
F 3 "~" V 3150 4450 50  0001 C CNN
	1    3150 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW37
U 1 1 5F86BB52
P 2750 4850
F 0 "SW37" H 2750 5135 50  0000 C CNN
F 1 "SW_Push" H 2750 5044 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 2750 5050 50  0001 C CNN
F 3 "~" H 2750 5050 50  0001 C CNN
	1    2750 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D37
U 1 1 5F86BB5C
P 2550 4950
F 0 "D37" V 2596 4880 50  0000 R CNN
F 1 "D" V 2505 4880 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 2550 4950 50  0001 C CNN
F 3 "~" V 2550 4950 50  0001 C CNN
	1    2550 4950
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW38
U 1 1 5F86BB66
P 3350 4850
F 0 "SW38" H 3350 5135 50  0000 C CNN
F 1 "SW_Push" H 3350 5044 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 3350 5050 50  0001 C CNN
F 3 "~" H 3350 5050 50  0001 C CNN
	1    3350 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D38
U 1 1 5F86BB70
P 3150 4950
F 0 "D38" V 3196 4880 50  0000 R CNN
F 1 "D" V 3105 4880 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 3150 4950 50  0001 C CNN
F 3 "~" V 3150 4950 50  0001 C CNN
	1    3150 4950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2950 4350 2950 4850
Wire Wire Line
	3550 4350 3550 4850
Wire Wire Line
	2550 4550 3150 4550
Wire Wire Line
	2550 5050 3150 5050
Wire Wire Line
	2950 3850 2950 4350
Connection ~ 2950 3850
Connection ~ 2950 4350
Wire Wire Line
	3550 3850 3550 4350
Connection ~ 3550 3850
Connection ~ 3550 4350
Wire Wire Line
	3150 3550 3750 3550
Connection ~ 3150 3550
Connection ~ 3750 3550
Wire Wire Line
	3150 4050 3750 4050
Connection ~ 3150 4050
Connection ~ 3750 4050
Wire Wire Line
	3150 4550 3750 4550
Connection ~ 3150 4550
Connection ~ 3750 4550
Wire Wire Line
	3150 5050 3750 5050
Connection ~ 3150 5050
Connection ~ 3750 5050
$Comp
L Switch:SW_Push SW5
U 1 1 5F8B520D
P 5150 3350
F 0 "SW5" H 5150 3635 50  0000 C CNN
F 1 "SW_Push" H 5150 3544 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 5150 3550 50  0001 C CNN
F 3 "~" H 5150 3550 50  0001 C CNN
	1    5150 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D5
U 1 1 5F8B5213
P 4950 3450
F 0 "D5" V 4996 3380 50  0000 R CNN
F 1 "D" V 4905 3380 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 4950 3450 50  0001 C CNN
F 3 "~" V 4950 3450 50  0001 C CNN
	1    4950 3450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW6
U 1 1 5F8B5219
P 5750 3350
F 0 "SW6" H 5750 3635 50  0000 C CNN
F 1 "SW_Push" H 5750 3544 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 5750 3550 50  0001 C CNN
F 3 "~" H 5750 3550 50  0001 C CNN
	1    5750 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D6
U 1 1 5F8B521F
P 5550 3450
F 0 "D6" V 5596 3380 50  0000 R CNN
F 1 "D" V 5505 3380 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 5550 3450 50  0001 C CNN
F 3 "~" V 5550 3450 50  0001 C CNN
	1    5550 3450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW17
U 1 1 5F8B5225
P 5150 3850
F 0 "SW17" H 5150 4135 50  0000 C CNN
F 1 "SW_Push" H 5150 4044 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 5150 4050 50  0001 C CNN
F 3 "~" H 5150 4050 50  0001 C CNN
	1    5150 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D17
U 1 1 5F8B522B
P 4950 3950
F 0 "D17" V 4996 3880 50  0000 R CNN
F 1 "D" V 4905 3880 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 4950 3950 50  0001 C CNN
F 3 "~" V 4950 3950 50  0001 C CNN
	1    4950 3950
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW18
U 1 1 5F8B5231
P 5750 3850
F 0 "SW18" H 5750 4135 50  0000 C CNN
F 1 "SW_Push" H 5750 4044 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 5750 4050 50  0001 C CNN
F 3 "~" H 5750 4050 50  0001 C CNN
	1    5750 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D18
U 1 1 5F8B5237
P 5550 3950
F 0 "D18" V 5596 3880 50  0000 R CNN
F 1 "D" V 5505 3880 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 5550 3950 50  0001 C CNN
F 3 "~" V 5550 3950 50  0001 C CNN
	1    5550 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5350 3350 5350 3850
Wire Wire Line
	5950 3350 5950 3850
Wire Wire Line
	4950 3550 5550 3550
Wire Wire Line
	4950 4050 5550 4050
$Comp
L Switch:SW_Push SW29
U 1 1 5F8B5241
P 5150 4350
F 0 "SW29" H 5150 4635 50  0000 C CNN
F 1 "SW_Push" H 5150 4544 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 5150 4550 50  0001 C CNN
F 3 "~" H 5150 4550 50  0001 C CNN
	1    5150 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D29
U 1 1 5F8B5247
P 4950 4450
F 0 "D29" V 4996 4380 50  0000 R CNN
F 1 "D" V 4905 4380 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 4950 4450 50  0001 C CNN
F 3 "~" V 4950 4450 50  0001 C CNN
	1    4950 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW30
U 1 1 5F8B524D
P 5750 4350
F 0 "SW30" H 5750 4635 50  0000 C CNN
F 1 "SW_Push" H 5750 4544 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 5750 4550 50  0001 C CNN
F 3 "~" H 5750 4550 50  0001 C CNN
	1    5750 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D30
U 1 1 5F8B5253
P 5550 4450
F 0 "D30" V 5596 4380 50  0000 R CNN
F 1 "D" V 5505 4380 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 5550 4450 50  0001 C CNN
F 3 "~" V 5550 4450 50  0001 C CNN
	1    5550 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW41
U 1 1 5F8B5259
P 5150 4850
F 0 "SW41" H 5150 5135 50  0000 C CNN
F 1 "SW_Push" H 5150 5044 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 5150 5050 50  0001 C CNN
F 3 "~" H 5150 5050 50  0001 C CNN
	1    5150 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D41
U 1 1 5F8B525F
P 4950 4950
F 0 "D41" V 4996 4880 50  0000 R CNN
F 1 "D" V 4905 4880 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 4950 4950 50  0001 C CNN
F 3 "~" V 4950 4950 50  0001 C CNN
	1    4950 4950
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW42
U 1 1 5F8B5265
P 5750 4850
F 0 "SW42" H 5750 5135 50  0000 C CNN
F 1 "SW_Push" H 5750 5044 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 5750 5050 50  0001 C CNN
F 3 "~" H 5750 5050 50  0001 C CNN
	1    5750 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D42
U 1 1 5F8B526B
P 5550 4950
F 0 "D42" V 5596 4880 50  0000 R CNN
F 1 "D" V 5505 4880 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 5550 4950 50  0001 C CNN
F 3 "~" V 5550 4950 50  0001 C CNN
	1    5550 4950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5350 4350 5350 4850
Wire Wire Line
	5950 4350 5950 4850
Wire Wire Line
	4950 4550 5550 4550
Wire Wire Line
	4950 5050 5550 5050
Wire Wire Line
	5350 3850 5350 4350
Connection ~ 5350 3850
Connection ~ 5350 4350
Wire Wire Line
	5950 3850 5950 4350
Connection ~ 5950 3850
Connection ~ 5950 4350
Wire Wire Line
	4350 3550 4950 3550
Connection ~ 4350 3550
Connection ~ 4950 3550
Wire Wire Line
	4350 4050 4950 4050
Connection ~ 4350 4050
Connection ~ 4950 4050
Wire Wire Line
	4350 4550 4950 4550
Connection ~ 4350 4550
Connection ~ 4950 4550
Wire Wire Line
	4350 5050 4950 5050
Connection ~ 4350 5050
Connection ~ 4950 5050
$Comp
L Switch:SW_Push SW9
U 1 1 5F901CC6
P 7550 3350
F 0 "SW9" H 7550 3635 50  0000 C CNN
F 1 "SW_Push" H 7550 3544 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 7550 3550 50  0001 C CNN
F 3 "~" H 7550 3550 50  0001 C CNN
	1    7550 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D9
U 1 1 5F901CCC
P 7350 3450
F 0 "D9" V 7396 3380 50  0000 R CNN
F 1 "D" V 7305 3380 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 7350 3450 50  0001 C CNN
F 3 "~" V 7350 3450 50  0001 C CNN
	1    7350 3450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW10
U 1 1 5F901CD2
P 8150 3350
F 0 "SW10" H 8150 3635 50  0000 C CNN
F 1 "SW_Push" H 8150 3544 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 8150 3550 50  0001 C CNN
F 3 "~" H 8150 3550 50  0001 C CNN
	1    8150 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D10
U 1 1 5F901CD8
P 7950 3450
F 0 "D10" V 7996 3380 50  0000 R CNN
F 1 "D" V 7905 3380 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 7950 3450 50  0001 C CNN
F 3 "~" V 7950 3450 50  0001 C CNN
	1    7950 3450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW21
U 1 1 5F901CDE
P 7550 3850
F 0 "SW21" H 7550 4135 50  0000 C CNN
F 1 "SW_Push" H 7550 4044 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 7550 4050 50  0001 C CNN
F 3 "~" H 7550 4050 50  0001 C CNN
	1    7550 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D21
U 1 1 5F901CE4
P 7350 3950
F 0 "D21" V 7396 3880 50  0000 R CNN
F 1 "D" V 7305 3880 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 7350 3950 50  0001 C CNN
F 3 "~" V 7350 3950 50  0001 C CNN
	1    7350 3950
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW22
U 1 1 5F901CEA
P 8150 3850
F 0 "SW22" H 8150 4135 50  0000 C CNN
F 1 "SW_Push" H 8150 4044 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 8150 4050 50  0001 C CNN
F 3 "~" H 8150 4050 50  0001 C CNN
	1    8150 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D22
U 1 1 5F901CF0
P 7950 3950
F 0 "D22" V 7996 3880 50  0000 R CNN
F 1 "D" V 7905 3880 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 7950 3950 50  0001 C CNN
F 3 "~" V 7950 3950 50  0001 C CNN
	1    7950 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7750 3350 7750 3850
Wire Wire Line
	8350 3350 8350 3850
Wire Wire Line
	7350 3550 7950 3550
Wire Wire Line
	7350 4050 7950 4050
$Comp
L Switch:SW_Push SW33
U 1 1 5F901CFA
P 7550 4350
F 0 "SW33" H 7550 4635 50  0000 C CNN
F 1 "SW_Push" H 7550 4544 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 7550 4550 50  0001 C CNN
F 3 "~" H 7550 4550 50  0001 C CNN
	1    7550 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D33
U 1 1 5F901D00
P 7350 4450
F 0 "D33" V 7396 4380 50  0000 R CNN
F 1 "D" V 7305 4380 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 7350 4450 50  0001 C CNN
F 3 "~" V 7350 4450 50  0001 C CNN
	1    7350 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW34
U 1 1 5F901D06
P 8150 4350
F 0 "SW34" H 8150 4635 50  0000 C CNN
F 1 "SW_Push" H 8150 4544 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 8150 4550 50  0001 C CNN
F 3 "~" H 8150 4550 50  0001 C CNN
	1    8150 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D34
U 1 1 5F901D0C
P 7950 4450
F 0 "D34" V 7996 4380 50  0000 R CNN
F 1 "D" V 7905 4380 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 7950 4450 50  0001 C CNN
F 3 "~" V 7950 4450 50  0001 C CNN
	1    7950 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW45
U 1 1 5F901D12
P 7550 4850
F 0 "SW45" H 7550 5135 50  0000 C CNN
F 1 "SW_Push" H 7550 5044 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 7550 5050 50  0001 C CNN
F 3 "~" H 7550 5050 50  0001 C CNN
	1    7550 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D45
U 1 1 5F901D18
P 7350 4950
F 0 "D45" V 7396 4880 50  0000 R CNN
F 1 "D" V 7305 4880 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 7350 4950 50  0001 C CNN
F 3 "~" V 7350 4950 50  0001 C CNN
	1    7350 4950
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW46
U 1 1 5F901D1E
P 8150 4850
F 0 "SW46" H 8150 5135 50  0000 C CNN
F 1 "SW_Push" H 8150 5044 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 8150 5050 50  0001 C CNN
F 3 "~" H 8150 5050 50  0001 C CNN
	1    8150 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D46
U 1 1 5F901D24
P 7950 4950
F 0 "D46" V 7996 4880 50  0000 R CNN
F 1 "D" V 7905 4880 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 7950 4950 50  0001 C CNN
F 3 "~" V 7950 4950 50  0001 C CNN
	1    7950 4950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7750 4350 7750 4850
Wire Wire Line
	8350 4350 8350 4850
Wire Wire Line
	7350 4550 7950 4550
Wire Wire Line
	7350 5050 7950 5050
Wire Wire Line
	7750 3850 7750 4350
Connection ~ 7750 3850
Connection ~ 7750 4350
Wire Wire Line
	8350 3850 8350 4350
Connection ~ 8350 3850
Connection ~ 8350 4350
$Comp
L Switch:SW_Push SW7
U 1 1 5F901D34
P 6350 3350
F 0 "SW7" H 6350 3635 50  0000 C CNN
F 1 "SW_Push" H 6350 3544 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 6350 3550 50  0001 C CNN
F 3 "~" H 6350 3550 50  0001 C CNN
	1    6350 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D7
U 1 1 5F901D3A
P 6150 3450
F 0 "D7" V 6196 3380 50  0000 R CNN
F 1 "D" V 6105 3380 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 6150 3450 50  0001 C CNN
F 3 "~" V 6150 3450 50  0001 C CNN
	1    6150 3450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW8
U 1 1 5F901D40
P 6950 3350
F 0 "SW8" H 6950 3635 50  0000 C CNN
F 1 "SW_Push" H 6950 3544 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 6950 3550 50  0001 C CNN
F 3 "~" H 6950 3550 50  0001 C CNN
	1    6950 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D8
U 1 1 5F901D46
P 6750 3450
F 0 "D8" V 6796 3380 50  0000 R CNN
F 1 "D" V 6705 3380 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 6750 3450 50  0001 C CNN
F 3 "~" V 6750 3450 50  0001 C CNN
	1    6750 3450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW19
U 1 1 5F901D4C
P 6350 3850
F 0 "SW19" H 6350 4135 50  0000 C CNN
F 1 "SW_Push" H 6350 4044 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 6350 4050 50  0001 C CNN
F 3 "~" H 6350 4050 50  0001 C CNN
	1    6350 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D19
U 1 1 5F901D52
P 6150 3950
F 0 "D19" V 6196 3880 50  0000 R CNN
F 1 "D" V 6105 3880 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 6150 3950 50  0001 C CNN
F 3 "~" V 6150 3950 50  0001 C CNN
	1    6150 3950
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW20
U 1 1 5F901D58
P 6950 3850
F 0 "SW20" H 6950 4135 50  0000 C CNN
F 1 "SW_Push" H 6950 4044 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 6950 4050 50  0001 C CNN
F 3 "~" H 6950 4050 50  0001 C CNN
	1    6950 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D20
U 1 1 5F901D5E
P 6750 3950
F 0 "D20" V 6796 3880 50  0000 R CNN
F 1 "D" V 6705 3880 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 6750 3950 50  0001 C CNN
F 3 "~" V 6750 3950 50  0001 C CNN
	1    6750 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6550 3350 6550 3850
Wire Wire Line
	7150 3350 7150 3850
Wire Wire Line
	6150 3550 6750 3550
Wire Wire Line
	6150 4050 6750 4050
$Comp
L Switch:SW_Push SW31
U 1 1 5F901D68
P 6350 4350
F 0 "SW31" H 6350 4635 50  0000 C CNN
F 1 "SW_Push" H 6350 4544 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 6350 4550 50  0001 C CNN
F 3 "~" H 6350 4550 50  0001 C CNN
	1    6350 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D31
U 1 1 5F901D6E
P 6150 4450
F 0 "D31" V 6196 4380 50  0000 R CNN
F 1 "D" V 6105 4380 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 6150 4450 50  0001 C CNN
F 3 "~" V 6150 4450 50  0001 C CNN
	1    6150 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW32
U 1 1 5F901D74
P 6950 4350
F 0 "SW32" H 6950 4635 50  0000 C CNN
F 1 "SW_Push" H 6950 4544 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 6950 4550 50  0001 C CNN
F 3 "~" H 6950 4550 50  0001 C CNN
	1    6950 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D32
U 1 1 5F901D7A
P 6750 4450
F 0 "D32" V 6796 4380 50  0000 R CNN
F 1 "D" V 6705 4380 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 6750 4450 50  0001 C CNN
F 3 "~" V 6750 4450 50  0001 C CNN
	1    6750 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW43
U 1 1 5F901D80
P 6350 4850
F 0 "SW43" H 6350 5135 50  0000 C CNN
F 1 "SW_Push" H 6350 5044 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 6350 5050 50  0001 C CNN
F 3 "~" H 6350 5050 50  0001 C CNN
	1    6350 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D43
U 1 1 5F901D86
P 6150 4950
F 0 "D43" V 6196 4880 50  0000 R CNN
F 1 "D" V 6105 4880 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 6150 4950 50  0001 C CNN
F 3 "~" V 6150 4950 50  0001 C CNN
	1    6150 4950
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW44
U 1 1 5F901D8C
P 6950 4850
F 0 "SW44" H 6950 5135 50  0000 C CNN
F 1 "SW_Push" H 6950 5044 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 6950 5050 50  0001 C CNN
F 3 "~" H 6950 5050 50  0001 C CNN
	1    6950 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D44
U 1 1 5F901D92
P 6750 4950
F 0 "D44" V 6796 4880 50  0000 R CNN
F 1 "D" V 6705 4880 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 6750 4950 50  0001 C CNN
F 3 "~" V 6750 4950 50  0001 C CNN
	1    6750 4950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6550 4350 6550 4850
Wire Wire Line
	7150 4350 7150 4850
Wire Wire Line
	6150 4550 6750 4550
Wire Wire Line
	6150 5050 6750 5050
Wire Wire Line
	6550 3850 6550 4350
Connection ~ 6550 3850
Connection ~ 6550 4350
Wire Wire Line
	7150 3850 7150 4350
Connection ~ 7150 3850
Connection ~ 7150 4350
Wire Wire Line
	6750 3550 7350 3550
Connection ~ 6750 3550
Connection ~ 7350 3550
Wire Wire Line
	6750 4050 7350 4050
Connection ~ 6750 4050
Connection ~ 7350 4050
Wire Wire Line
	6750 4550 7350 4550
Connection ~ 6750 4550
Connection ~ 7350 4550
Wire Wire Line
	6750 5050 7350 5050
Connection ~ 6750 5050
Connection ~ 7350 5050
$Comp
L Switch:SW_Push SW11
U 1 1 5F901DAE
P 8750 3350
F 0 "SW11" H 8750 3635 50  0000 C CNN
F 1 "SW_Push" H 8750 3544 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 8750 3550 50  0001 C CNN
F 3 "~" H 8750 3550 50  0001 C CNN
	1    8750 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D11
U 1 1 5F901DB4
P 8550 3450
F 0 "D11" V 8596 3380 50  0000 R CNN
F 1 "D" V 8505 3380 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 8550 3450 50  0001 C CNN
F 3 "~" V 8550 3450 50  0001 C CNN
	1    8550 3450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW12
U 1 1 5F901DBA
P 9350 3350
F 0 "SW12" H 9350 3635 50  0000 C CNN
F 1 "SW_Push" H 9350 3544 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 9350 3550 50  0001 C CNN
F 3 "~" H 9350 3550 50  0001 C CNN
	1    9350 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D12
U 1 1 5F901DC0
P 9150 3450
F 0 "D12" V 9196 3380 50  0000 R CNN
F 1 "D" V 9105 3380 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 9150 3450 50  0001 C CNN
F 3 "~" V 9150 3450 50  0001 C CNN
	1    9150 3450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW23
U 1 1 5F901DC6
P 8750 3850
F 0 "SW23" H 8750 4135 50  0000 C CNN
F 1 "SW_Push" H 8750 4044 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 8750 4050 50  0001 C CNN
F 3 "~" H 8750 4050 50  0001 C CNN
	1    8750 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D23
U 1 1 5F901DCC
P 8550 3950
F 0 "D23" V 8596 3880 50  0000 R CNN
F 1 "D" V 8505 3880 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 8550 3950 50  0001 C CNN
F 3 "~" V 8550 3950 50  0001 C CNN
	1    8550 3950
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW24
U 1 1 5F901DD2
P 9350 3850
F 0 "SW24" H 9350 4135 50  0000 C CNN
F 1 "SW_Push" H 9350 4044 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 9350 4050 50  0001 C CNN
F 3 "~" H 9350 4050 50  0001 C CNN
	1    9350 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D24
U 1 1 5F901DD8
P 9150 3950
F 0 "D24" V 9196 3880 50  0000 R CNN
F 1 "D" V 9105 3880 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 9150 3950 50  0001 C CNN
F 3 "~" V 9150 3950 50  0001 C CNN
	1    9150 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8950 3350 8950 3850
Wire Wire Line
	9550 3350 9550 3850
Wire Wire Line
	8550 3550 9150 3550
Wire Wire Line
	8550 4050 9150 4050
$Comp
L Switch:SW_Push SW35
U 1 1 5F901DE2
P 8750 4350
F 0 "SW35" H 8750 4635 50  0000 C CNN
F 1 "SW_Push" H 8750 4544 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 8750 4550 50  0001 C CNN
F 3 "~" H 8750 4550 50  0001 C CNN
	1    8750 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D35
U 1 1 5F901DE8
P 8550 4450
F 0 "D35" V 8596 4380 50  0000 R CNN
F 1 "D" V 8505 4380 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 8550 4450 50  0001 C CNN
F 3 "~" V 8550 4450 50  0001 C CNN
	1    8550 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW36
U 1 1 5F901DEE
P 9350 4350
F 0 "SW36" H 9350 4635 50  0000 C CNN
F 1 "SW_Push" H 9350 4544 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 9350 4550 50  0001 C CNN
F 3 "~" H 9350 4550 50  0001 C CNN
	1    9350 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D36
U 1 1 5F901DF4
P 9150 4450
F 0 "D36" V 9196 4380 50  0000 R CNN
F 1 "D" V 9105 4380 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 9150 4450 50  0001 C CNN
F 3 "~" V 9150 4450 50  0001 C CNN
	1    9150 4450
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW47
U 1 1 5F901DFA
P 8750 4850
F 0 "SW47" H 8750 5135 50  0000 C CNN
F 1 "SW_Push" H 8750 5044 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 8750 5050 50  0001 C CNN
F 3 "~" H 8750 5050 50  0001 C CNN
	1    8750 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D47
U 1 1 5F901E00
P 8550 4950
F 0 "D47" V 8596 4880 50  0000 R CNN
F 1 "D" V 8505 4880 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 8550 4950 50  0001 C CNN
F 3 "~" V 8550 4950 50  0001 C CNN
	1    8550 4950
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW48
U 1 1 5F901E06
P 9350 4850
F 0 "SW48" H 9350 5135 50  0000 C CNN
F 1 "SW_Push" H 9350 5044 50  0001 C CNN
F 2 "MX_Only:MXOnly-1U-Hotswap" H 9350 5050 50  0001 C CNN
F 3 "~" H 9350 5050 50  0001 C CNN
	1    9350 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Small D48
U 1 1 5F901E0C
P 9150 4950
F 0 "D48" V 9196 4880 50  0000 R CNN
F 1 "D" V 9105 4880 50  0001 R CNN
F 2 "Diode_SMD:D_SOD-123" V 9150 4950 50  0001 C CNN
F 3 "~" V 9150 4950 50  0001 C CNN
	1    9150 4950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8950 4350 8950 4850
Wire Wire Line
	9550 4350 9550 4850
Wire Wire Line
	8550 4550 9150 4550
Wire Wire Line
	8550 5050 9150 5050
Wire Wire Line
	8950 3850 8950 4350
Connection ~ 8950 3850
Connection ~ 8950 4350
Wire Wire Line
	9550 3850 9550 4350
Connection ~ 9550 3850
Connection ~ 9550 4350
Wire Wire Line
	7950 3550 8550 3550
Connection ~ 7950 3550
Connection ~ 8550 3550
Wire Wire Line
	7950 4050 8550 4050
Connection ~ 7950 4050
Connection ~ 8550 4050
Wire Wire Line
	7950 4550 8550 4550
Connection ~ 7950 4550
Connection ~ 8550 4550
Wire Wire Line
	7950 5050 8550 5050
Connection ~ 7950 5050
Connection ~ 8550 5050
Wire Wire Line
	5550 3550 6150 3550
Connection ~ 5550 3550
Connection ~ 6150 3550
Wire Wire Line
	5550 4050 6150 4050
Connection ~ 5550 4050
Connection ~ 6150 4050
Wire Wire Line
	5550 4550 6150 4550
Connection ~ 5550 4550
Connection ~ 6150 4550
Wire Wire Line
	5550 5050 6150 5050
Connection ~ 5550 5050
Connection ~ 6150 5050
Text GLabel 2950 2900 1    50   Input ~ 0
C0
Text GLabel 3550 2900 1    50   Input ~ 0
C1
Text GLabel 4150 2900 1    50   Input ~ 0
C2
Text GLabel 4750 2900 1    50   Input ~ 0
C3
Text GLabel 5350 2900 1    50   Input ~ 0
C4
Text GLabel 5950 2900 1    50   Input ~ 0
C5
Text GLabel 6550 2900 1    50   Input ~ 0
C6
Text GLabel 7150 2900 1    50   Input ~ 0
C7
Text GLabel 7750 2900 1    50   Input ~ 0
C8
Text GLabel 8350 2900 1    50   Input ~ 0
C9
Text GLabel 8950 2900 1    50   Input ~ 0
C10
Text GLabel 9550 2900 1    50   Input ~ 0
C11
Wire Wire Line
	2950 2900 2950 3350
Connection ~ 2950 3350
Wire Wire Line
	3550 2900 3550 3350
Connection ~ 3550 3350
Wire Wire Line
	4150 2900 4150 3350
Connection ~ 4150 3350
Wire Wire Line
	4750 2900 4750 3350
Connection ~ 4750 3350
Wire Wire Line
	5350 2900 5350 3350
Connection ~ 5350 3350
Wire Wire Line
	5950 2900 5950 3350
Connection ~ 5950 3350
Wire Wire Line
	6550 2900 6550 3350
Connection ~ 6550 3350
Wire Wire Line
	7150 2900 7150 3350
Connection ~ 7150 3350
Wire Wire Line
	7750 2900 7750 3350
Connection ~ 7750 3350
Wire Wire Line
	8350 2900 8350 3350
Connection ~ 8350 3350
Wire Wire Line
	8950 2900 8950 3350
Connection ~ 8950 3350
Wire Wire Line
	9550 2900 9550 3350
Connection ~ 9550 3350
Text GLabel 2200 3550 0    50   Input ~ 0
R0
Text GLabel 2200 4050 0    50   Input ~ 0
R1
Text GLabel 2200 4550 0    50   Input ~ 0
R2
Text GLabel 2200 5050 0    50   Input ~ 0
R3
Wire Wire Line
	2200 3550 2550 3550
Connection ~ 2550 3550
Wire Wire Line
	2200 4050 2550 4050
Connection ~ 2550 4050
Wire Wire Line
	2200 4550 2550 4550
Connection ~ 2550 4550
Wire Wire Line
	2200 5050 2550 5050
Connection ~ 2550 5050
Text GLabel 4450 7050 0    50   Input ~ 0
C11
Text GLabel 4450 6950 0    50   Input ~ 0
C10
$Comp
L Connector:Conn_01x12_Female J2
U 1 1 5FA1E26F
P 4650 6450
F 0 "J2" V 4815 6380 50  0000 C CNN
F 1 "Conn_01x12_Female" V 4724 6380 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x12_P2.54mm_Vertical" H 4650 6450 50  0001 C CNN
F 3 "~" H 4650 6450 50  0001 C CNN
	1    4650 6450
	1    0    0    -1  
$EndComp
$EndSCHEMATC
