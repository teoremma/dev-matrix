# dev-matrix

Development keyboard matrix for experimentation.

![dev-matrix](./images/render_front.png)

This bare-bones keyboard matrix is intended to aid in the development of keyboard
software and hardware. It has it's rows and columns broken out to allow for easy
connection to a breadboard or a microcontroller (Arduino, Teensy, BluePill, etc.).
It has diodes arranged in a column-to-row manner to prevent ghosting, as is standard.

